<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('home');
})->name('home');

Route::livewire('/quiz', 'quiz')->name('quiz');
Route::get('results/{id}', 'ResultsController');
Route::get('groups', 'GroupsController');
Route::livewire('study/{id}', 'study')->name('study');

Route::get('/login', 'LoginLogoutController@login')->name('login');
Route::post('/login', 'LoginLogoutController@store');
Route::get('/logout', 'LoginLogoutController@logout')->name('logout');
Route::get('/loggedout', function () {
  return view('loggedout');
})->name('loggedout');
