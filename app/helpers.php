<?php

function user_is_logged_in(): bool
{
  $student = session('student');
  return $student->id ? true : false;
}

function user_is_not_logged_in(): bool
{
  $student = session('student');
  return $student->id ? false : true;
}

function user_logged_in_name(): string
{
  $student = session('student');
  return $student->name;
}

function user_logged_in_id(): int
{
  $student = session('student');
  return $student->id;
}
