<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DogAPI
{
  public $url;

  public function __construct()
  {
    $this->url = 'https://dog.ceo/api';
  }

  public function getBreeds(): Object
  {
    $query_string = $this->url . '/breeds/list/all';
    $response = Http::get($query_string)->getBody();
    $message = json_decode((string) $response);
    return $message;
  }

  public function getImage(string $breed, string $subbreed): string
  {
    $query_string = null;
    if (Str::of($subbreed)->trim()->isEmpty()) {
      $query_string = $this->url . "/breed/$breed/images/random";
    } else {
      $query_string = $this->url . "/breed/$breed/$subbreed/images/random";
    }
    $response = Http::get($query_string)->getBody();
    $message = json_decode((string) $response);
    return $message->message;
  }
}
