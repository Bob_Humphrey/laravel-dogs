<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DogAPIFacade extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'dog-api';
  }
}
