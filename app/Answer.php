<?php

namespace App;

use App\Actions\GetAnswersByUserAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use App\Breed;
use App\Actions\UpdateAnswerAction;

class Answer extends Model
{
  public function breed()
  {
    return $this->belongsTo(Breed::class);
  }

  public static function getAnswersByUser($userId): Collection
  {
    return GetAnswersByUserAction::execute($userId);
  }

  public static function updateAnswer($breedId, $correct): void
  {
    UpdateAnswerAction::execute($breedId, $correct);
  }
}
