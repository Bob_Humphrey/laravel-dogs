<?php

namespace App\Actions;

class ProtectResultsAction
{
  public static function execute($userId): bool
  {
    if (user_is_not_logged_in()) {
      session()->flash('message_type', 'bad');
      session()->flash('message_content', 'You must be logged in to see your results.');
      return false;
    }

    $id = user_logged_in_id();
    if ($userId != $id) {
      session()->flash('message_type', 'bad');
      session()->flash('message_content', 'You can only see results for ' . user_logged_in_name() . '.');
      return false;
    }

    return true;
  }
}
