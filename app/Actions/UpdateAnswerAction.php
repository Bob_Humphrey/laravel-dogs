<?php

namespace App\Actions;

use App\Answer;

class UpdateAnswerAction
{

  public static function execute($breedId, $correct): void
  {
    $userId = user_logged_in_id();
    $record = Answer::where('breed_id', $breedId)
      ->where('user_id', $userId)
      ->first();

    if (empty($record)) {
      $answer = new Answer();
      if ($correct) {
        $answer->right = 1;
        $answer->wrong = 0;
        $answer->wrong_pct = 0.00;
      } else {
        $answer->right = 0;
        $answer->wrong = 1;
        $answer->wrong_pct = 1.00;
      }
      $answer->breed_id = $breedId;
      $answer->user_id = $userId;
      $answer->save();
      return;
    }

    if ($correct) {
      $record->right++;
    } else {
      $record->wrong++;
    }
    $record->wrong_pct = floatval($record->wrong / ($record->right + $record->wrong));
    $record->breed_id = $breedId;
    $record->user_id = $userId;
    $record->save();
  }
}
