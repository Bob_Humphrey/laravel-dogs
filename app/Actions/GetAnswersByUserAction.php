<?php

namespace App\Actions;

use Illuminate\Database\Eloquent\Collection;
use App\Answer;

class GetAnswersByUserAction
{
  public static function execute($userId): Collection
  {
    $answers = Answer::select('breed_id', 'wrong_pct', 'right', 'wrong')
      ->with('breed')
      ->where('user_id', $userId)
      ->orderBy('wrong_pct', 'DESC')
      ->orderBy('wrong', 'DESC')
      ->orderBy('right', 'ASC')
      ->get();
    return $answers;
  }
}
