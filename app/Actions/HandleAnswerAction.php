<?php

namespace App\Actions;

use App\Answer;
use Illuminate\Support\Str;

class HandleAnswerAction
{

  public static function execute($state, $userAnswer): array
  {
    if ($state['handleAnswerMode'] === true) {
      return $state;
    }

    $state['userAnswer'] = $userAnswer;

    if ($state['correctChoice']['id'] === $userAnswer) {
      $state['correct'] = true;
      $state['rightAnswers']++;
    } else {
      $state['correct'] = false;
      $state['wrongAnswers']++;
    }

    $response = [];
    $response['id'] = $state['correctChoice']['id'];
    $response['breed'] = $state['correctChoice']['name'];
    $response['correct'] = $state['correct'];
    array_unshift($state['responses'], $response);

    Answer::updateAnswer($state['correctChoice']['id'], 0, $state['correct']);

    $correct = $state['correct'] ? 'Right' : 'Wrong';
    $rightAnswerLabel = Str::of('answer')->plural($state['rightAnswers']);
    $wrongAnswerLabel = Str::of('answer')->plural($state['wrongAnswers']);
    $rightAnswers = $state['rightAnswers'];
    $wrongAnswers = $state['wrongAnswers'];
    $correctBreed = $state['correctChoice']['name'];
    $correctId = $state['correctChoice']['id'];
    $url = url('study/' . $correctId);
    $state['feedback'] = "<span class='font-nunito_bold'>$correct</span>. 
    The correct answer is <a href='$url' class='font-nunito_bold text-brown-500 cursor-pointer'>$correctBreed</a>.</br>You have $rightAnswers right $rightAnswerLabel and $wrongAnswers wrong $wrongAnswerLabel.";

    $state['handleAnswerMode'] = true;
    session([
      'responses' => $state['responses'],
      'rightAnswers' => $state['rightAnswers'],
      'wrongAnswers' => $state['wrongAnswers'],
    ]);
    request()->session()->forget('question');

    return $state;
  }
}
