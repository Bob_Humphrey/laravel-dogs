<?php

namespace App\Actions;

class GetStudyBreedAction
{
  public static function execute($breedId): array
  {
    $breed = collect(session('breeds'))->firstWhere('id', $breedId);
    $api = app('dog-api');
    $image = $api->getImage($breed['breed'], $breed['subbreed']);
    return array('breed' => $breed, 'image' => $image);
  }
}
