<?php

namespace App\Actions;

use Illuminate\Support\Str;
use App\Breed;

class AddBreedsAction
{
  public static function execute(): void
  {
    // The list of breeds is returned from the API as an object. 
    // The key of the object attributes is the breed name. 
    // The value of the object attributes is an array of any
    // subbreeds associated with the breed, or an empty array
    // if there are no subbreeds associated with the breed.
    // 
    //    "boxer": [],
    //    "brabancon": [],
    //    "briard": [],
    //    "buhund": [
    //        "norwegian"
    //    ],
    //    "bulldog": [
    //        "boston",
    //        "english",
    //        "french"
    //    ],
    //    "bullterrier": [
    //        "staffordshire"
    //    ],

    $api = app('dog-api');
    $object = $api->getBreeds();
    $array = (array)$object->message;
    $breeds = [];

    foreach ($array as $key => $value) {
      if (count($value)) {
        foreach ($value as $subbreed) {
          $breed = [];
          $breed['breed'] = $key;
          $breed['subbreed'] = $subbreed;
          $breed['key'] = (string) Str::of($subbreed . ' ' . $key)->title();
          $breeds[] = $breed;
        }
      } else {
        $breed = [];
        $breed['breed'] = $key;
        $breed['subbreed'] = '';
        $breed['key'] = (string) Str::of($key)->title();
        $breeds[] = $breed;
      }
    }

    // Key and name are similar fields.  Name is occasionally
    // manually modified when the one provided by the DogAPI
    // needs improvement. For example, "Germanshepherd" is changed
    // to "German Shepherd".
    $collection = collect($breeds)->whereNotIn('breed', ['mix']);
    $collection->each(function ($item, $key) {
      $record = Breed::where('key', $item['key'])->first();
      if (empty($record)) {
        $breed = new Breed();
        $breed->breed = $item['breed'];
        $breed->subbreed = $item['subbreed'];
        $breed->name = AddBreedsAction::correctName($item['key']);
        $breed->key = $item['key'];
        $breed->lesser = AddBreedsAction::isLesserKnown($item['key']);
        $breed->hound = AddBreedsAction::isHound($item['key']);
        $breed->terrier = AddBreedsAction::isTerrier($item['key']);
        $breed->spaniel = AddBreedsAction::isSpaniel($item['key']);
        $breed->retriever = AddBreedsAction::isRetriever($item['key']);
        $breed->save();
      }
    });
  }

  private static function correctName($name): string
  {

    $names = [
      'Shepherd Australian' => 'Australian Shepherd',
      'Staffordshire Bullterrier' => 'Staffordshire Bull Terrier',
      'Australian Cattledog' => 'Australian Cattle Dog',
      'Cotondetulear' => 'Coton de Tulear',
      'Lapphund Finnish' => 'Finnish Lapphund',
      'Germanshepherd' => 'German Shepherd',
      'Mexicanhairless' => 'Mexican Hairless',
      'Germanlonghair Pointer' => 'German Longhair Pointer',
      'Stbernard' => 'St Bernard',
      'Kerryblue Terrier' => 'Kerry Blue Terrier',
      'Westhighland Terrier' => 'West Highland White Terrier',
      'Spanish Waterdog' => 'Spanish Water Dog',
    ];
    $collection = collect($names);
    $correctedName = $collection->get($name);
    if (is_null($correctedName)) {
      return $name;
    }
    return $correctedName;
  }

  private static function isLesserKnown($name): bool
  {
    $names = [
      'Affenpinscher', 'African', 'Appenzeller', 'Basenji', 'Bouvier', 'Brabancon', 'Briard', 'Cairn', 'Clumber', 'Cotondetulear', 'Dhole', 'Entlebucher', 'Groenendael', 'Havanese', 'Kelpie', 'Komondor', 'Kuvasz', 'Leonberg', 'Lhasa', 'Malinois', 'Bernese Mountain',
      'Swiss Mountain', 'Caucasian Ovcharka', 'Papillon', 'Pembroke', 'Germanlonghair Pointer', 'Puggle', 'Rhodesian Ridgeback', 'Saluki', 'Samoyed', 'Schipperke',
      'Shiba', 'Vizsla', 'Spanish Waterdog'
    ];
    $collection = collect($names);
    return $collection->contains($name);
  }

  private static function isHound($name): bool
  {
    $lower = Str::of($name)->lower();
    if (Str::of($lower)->contains('hound')) {
      return true;
    }
    if (Str::of($lower)->contains('hund')) {
      return true;
    }
    if (Str::of($lower)->contains('hond')) {
      return true;
    }
    return false;
  }

  private static function isTerrier($name): bool
  {
    $lower = Str::of($name)->lower();
    if (Str::of($lower)->contains('terrier')) {
      return true;
    }
    return false;
  }

  private static function isSpaniel($name): bool
  {
    $lower = Str::of($name)->lower();
    if (Str::of($lower)->contains('spaniel')) {
      return true;
    }
    return false;
  }

  private static function isRetriever($name): bool
  {
    $lower = Str::of($name)->lower();
    if (Str::of($lower)->contains('retriever')) {
      return true;
    }
    return false;
  }
}
