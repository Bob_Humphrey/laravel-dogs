<?php

namespace App\Actions;

class GetQuestionAction
{
  public static function execute($state): array
  {
    if (request()->session()->has('question')) {
      $question = session('question');
      $state = GetQuestionAction::updateState($state, $question);
      return $state;
    }

    $collection = collect(session('breeds'));
    $take = $collection->shuffle()->take(5);
    $correctChoiceCollection = $take->splice(4);
    $choices = $take->concat($correctChoiceCollection)->shuffle()->all();
    $correctChoice = $correctChoiceCollection[0];
    $api = app('dog-api');
    $image = $api->getImage($correctChoice['breed'], $correctChoice['subbreed']);
    $question = [
      'choices' => $choices,
      'correctChoice' => $correctChoice,
      'image' => $image
    ];
    session(['question' => $question]);
    $state = GetQuestionAction::updateState($state, $question);
    return $state;
  }

  private static function updateState($state, $question): array
  {
    $state['choices'] = $question['choices'];
    $state['image'] = $question['image'];
    $state['correctChoice'] = $question['correctChoice'];
    $state['handleAnswerMode'] = false;
    $state['userAnswer'] = 0;
    return $state;
  }
}
