<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Breed;


class UpdateBreeds extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_breeds';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Add new breeds to the database using the Dog API';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    Breed::add();
  }
}
