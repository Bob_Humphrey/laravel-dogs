<?php

namespace App;

use App\Answer;

use App\Actions\AddBreedsAction;
use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
  public function answers()
  {
    return $this->hasMany(Answer::class);
  }

  public static function add(): void
  {
    AddBreedsAction::execute();
  }
}
