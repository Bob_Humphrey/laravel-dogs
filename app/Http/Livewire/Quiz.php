<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\GetQuestionAction;
use App\Actions\HandleAnswerAction;

class Quiz extends Component
{
  public $state = [
    'choices' => [],
    'image' => '',
    'correctChoice' => null,
    'correct' => false,
    'userAnswer' => 0,
    'rightAnswers' => 0,
    'wrongAnswers' => 0,
    'handleAnswerMode' => false,
    'feedback' => '',
    'responses' => []
  ];

  protected function getQuestion(): void
  {
    $this->state = GetQuestionAction::execute($this->state);
  }

  public function handleAnswer($userAnswer)
  {
    $this->state = HandleAnswerAction::execute($this->state, $userAnswer);
  }

  public function handleQuitQuiz()
  {
    $this->state['responses'] = [];
    $this->state['rightAnswers'] = 0;
    $this->state['wrongAnswers'] = 0;
    session(['responses' => []]);
    session(['rightAnswers' => 0]);
    session(['wrongAnswers' => 0]);
    request()->session()->forget('question');
    redirect()->route('home');
  }

  public function nextQuestion(): void
  {
    $this->getQuestion();
  }

  public function mount(): void
  {
    if (request()->session()->has('responses')) {
      $this->state['responses'] = session('responses');
      $this->state['rightAnswers'] = session('rightAnswers');
      $this->state['wrongAnswers'] = session('wrongAnswers');
    }
    $this->getQuestion();
  }

  public function render(): object
  {
    return view('livewire.quiz');
  }
}
