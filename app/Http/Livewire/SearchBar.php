<?php

namespace App\Http\Livewire;

use App\Breed;
use Livewire\Component;

class SearchBar extends Component
{
  public $query;
  public $breeds;
  public $highlightIndex;

  public function mount()
  {
    $this->resetState();
  }

  public function resetState()
  {
    $this->query = '';
    $this->breeds = [];
    $this->highlightIndex = 0;
  }

  public function incrementHighlight()
  {
    if ($this->highlightIndex === count($this->contacts) - 1) {
      $this->highlightIndex = 0;
      return;
    }
    $this->highlightIndex++;
  }

  public function decrementHighlight()
  {
    if ($this->highlightIndex === 0) {
      $this->highlightIndex = count($this->contacts) - 1;
      return;
    }
    $this->highlightIndex--;
  }

  public function selectBreed()
  {
    $breed = $this->breeds[$this->highlightIndex] ?? null;
    if ($breed) {
      $this->redirect(route('study', $breed['id']));
    }
  }

  public function updatedQuery()
  {
    $this->breeds = Breed::where('name', 'like', '%' . $this->query . '%')
      ->get()
      ->toArray();
  }
  public function render()
  {
    return view('livewire.search-bar');
  }
}
