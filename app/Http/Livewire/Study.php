<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\GetStudyBreedAction;

class Study extends Component
{
  public int $breedId = 0;
  public $breed;
  public string $image;

  public function handleSeeAnother()
  {
    $breedData = GetStudyBreedAction::execute($this->breedId);
    $this->breed = $breedData['breed'];
    $this->image = $breedData['image'];
  }

  public function mount($id, GetStudyBreedAction $getStudyBreedAction): void
  {
    $this->breedId = $id;
    $breedData = $getStudyBreedAction->execute($id);
    $this->breed = $breedData['breed'];
    $this->image = $breedData['image'];
  }

  public function render()
  {
    return view('livewire.study');
  }
}
