<?php

namespace App\Http\Controllers;

use App\Actions\ProtectResultsAction;
use App\Answer;

class ResultsController extends Controller
{
  public function __invoke($userId, Answer $answer)
  {
    if (!ProtectResultsAction::execute($userId)) {
      return redirect()->route('home');
    }
    $answers = $answer::getAnswersByUser($userId);
    return view('results', ['answers' => $answers]);
  }
}
