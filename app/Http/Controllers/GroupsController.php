<?php

namespace App\Http\Controllers;

use App\Breed;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request, Breed $breed)
  {
    $lesserKnown = $breed::where('lesser', true)->get();
    $chunkLesserKnown = $lesserKnown->chunk(round($lesserKnown->count() / 3));
    $hounds = $breed::where('hound', true)->get();
    $chunkHounds = $hounds->chunk(round($hounds->count() / 3));
    $terriers = $breed::where('terrier', true)->get();
    $chunkTerriers = $terriers->chunk(round($terriers->count() / 3));
    $spaniels = $breed::where('spaniel', true)->get();
    $chunkSpaniels = $spaniels->chunk(round($spaniels->count() / 3));
    $retrievers = $breed::where('retriever', true)->get();
    $chunkRetrievers = $retrievers->chunk(round($retrievers->count() / 3));
    return view('groups', [
      'lesserKnown' => $chunkLesserKnown,
      'hounds' => $chunkHounds,
      'terriers' => $chunkTerriers,
      'spaniels' => $chunkSpaniels,
      'retrievers' => $chunkRetrievers,
    ]);
  }
}
