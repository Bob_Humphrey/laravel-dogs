<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class LoginLogoutController extends Controller
{
  public function login()
  {
    return view('login', []);
  }

  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'username' => 'required',
    ]);
    $username = Str::of($request->get('username'))->trim();

    $record = Student::whereName($username)->first();
    if (!empty($record)) {
      session(['student' => $record]);
    } else {
      $created = Student::create(['name' => $username]);
      session(['student' => $created]);
    }

    return redirect()->route('quiz');
  }

  public function logout()
  {
    $student = new Student;
    $student->id = 0;
    $student->name = 'Anonymous';
    request()->session()->put('student', $student);
    return redirect()->route('loggedout');
  }
}
