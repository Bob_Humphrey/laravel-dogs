<?php

namespace App\Http\Middleware;

use Closure;
use App\Breed;
use App\Student;
use Illuminate\Support\Facades\Http;

class InitializeSession
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (!$request->session()->has('student')) {
      $student = new Student;
      $student->id = 0;
      $student->name = 'Anonymous';
      $request->session()->put('student', $student);
    }

    if (!$request->session()->has('breeds')) {
      $collection = Breed::orderBy('name', 'ASC')->get();
      $breeds = $collection->all();
      $request->session()->put('breeds', $breeds);
    }

    return $next($request);
  }
}
