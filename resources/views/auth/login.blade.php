@extends('layouts.app')

@section('content')
  <div class=''>
    <div class='text-3xl font-nunito_bold text-indigo-700 mt-2'>Login</div>

    <div class='bg-gray-100 py-16 mt-6 mb-16 rounded-md'>
      <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class='grid grid-cols-2 font-nunito_regular text-lg'>
          <label for="email" class='col-span-1 text-right pr-6 py-2'>
            User Name
          </label>

          <div class='col-span-1'>
            <input id="email" type="email" class='px-4 py-2' name="email" value="{{ old('email') }}" required autofocus>
          </div>
        </div>

        <div class='grid grid-cols-2 font-nunito_regular text-lg'>
          <div class='col-span-1'>
          </div>

          <div class='col-span-1'>
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>


        <div class="flex items-center w-full pt-10">
          <button type="submit"
            class='hover:bg-gray-800 hover:text-white text-center font-nunito_bold rounded w-48 py-2 mx-auto border border-gray-700 cursor-pointer'>
            Login
          </button>
        </div>
      </form>
    </div>

  </div>
@endsection
