@extends('layouts.app')

@section('content')
  <div class=''>

    <div class="mb-6 px-24">
      <img src="{{ asset('img/dog2.png') }}" alt="Logo" />
    </div>
    <div class="text-3xl font-nunito_extrabold text-indigo-700 text-center mb-4">
      You're now logged out!
    </div>
    <div class="text-2xl font-nunito_bold text-gray-500 text-center mb-8">
      Your results will be remembered for the next time you login.
    </div>

  </div>
@endsection
