@extends('layouts.app')

@section('content')

  @if ($answers->isEmpty())
    <div class=''>
      <div class="mb-6 px-24">
        <img src="{{ asset('img/dog2.png') }}" alt="Logo" />
      </div>
      <div class="text-3xl font-nunito_extrabold text-indigo-700 text-center mb-4">
        You do not have any results yet.
      </div>
      <div class="text-2xl font-nunito_bold text-gray-500 text-center mb-8">
        Once you start answering quiz questions, your results will appear here.
      </div>
    </div>
  @endif

  @if (!$answers->isEmpty())
    <div class="font-nunito_bold text-base text-center py-4 text-gray-400">
      Results for All of the Quizes Taken by {{ user_logged_in_name() }}
    </div>
    <div class="flex w-full justify-center mx-auto pb-10">
      <table class="w-5/6">
        <tr class="grid grid-cols-12 text-sm font-nunito_bold border-b border-gray-300">
          <th class="col-span-3 text-left py-3">Breed</th>
          <th class="col-span-3 text-right py-3">Percent Wrong</th>
          <th class="col-span-3 text-right py-3">Right Answers</th>
          <th class="col-span-3 text-right py-3">Wrong Answers</th>
        </tr>
        @foreach ($answers as $answer)
          <tr class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300">
            <td class="col-span-3 text-left py-3">
              <a href={{ url('study/' . $answer->breed_id) }} class="font-nunito_bold text-brown-500 cursor-pointer">
                {{ $answer->breed->name }}
              </a>
            </td>
            <td class="col-span-3 text-right py-3">{{ $answer->wrong_pct * 100 }}</td>
            <td class="col-span-3 text-right py-3">{{ $answer->right }}</td>
            <td class="col-span-3 text-right py-3">{{ $answer->wrong }}</td>
          </tr>
        @endforeach
      </table>
    </div>
  @endif

@endsection
