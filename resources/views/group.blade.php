<div class="text-2xl font-nunito_extrabold text-indigo-800 text-center my-4">
  {{ $subtitle }}
</div>
<div class="md:grid grid-cols-3">
  @foreach ($chunks as $chunk)
    <div class="">
      @foreach ($chunk as $item)
        <div class="">
          <a href={{ url('study/' . $item->id) }} class="font-nunito_bold text-brown-500 cursor-pointer">
            {{ $item->name }}
          </a>
        </div>
      @endforeach
    </div>
  @endforeach
</div>
