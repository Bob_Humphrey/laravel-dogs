<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="PROJECT DESCRIPTION">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  @livewireScripts
  <script src="{{ asset('js/all.js') }}" defer></script>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>
  <div class="font-nunito_regular">
    <nav class="bg-gray-50 py-2" x-data="{ showMenu: false }">
      <div
        class="flex flex-col lg:flex-row lg:justify-between lg:items-center w-11/12 xl:w-5/6 mx-auto py-3 bg-purple-300 sm:bg-red-300 md:bg-orange-300 lg:bg-yellow-300 xl:bg-gray-50">

        <div class="flex justify-between items-center">

          <div class="flex lg:justify-start">
            <a href="/" class="flex flex-col lg:flex-row w-full">
              <h1 class="font-nunito_extrabold text-3xl text-indigo-700 hover:text-brown-500">
                {{ config('app.name', 'Laravel Application') }}
              </h1>
            </a>
          </div>

        </div>

      </div>
    </nav>

    <main class="w-11/12 lg:w-3/4 xl:w-1/2 py-4 mx-auto">
      @include('layouts.flash')
      <div class=''>

        <div class="mb-6 px-24">
          <img src="{{ asset('img/dog2.png') }}" alt="Logo" />
        </div>
        <div class="text-3xl font-nunito_extrabold text-indigo-700 text-center mb-4">
          Sorry, that page could not be found!
        </div>

      </div>
    </main>

    @include('layouts.footer')

  </div>
</body>

</html>
