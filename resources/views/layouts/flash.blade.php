@if (session()->has('message_type'))
  @php
  $bgColor = session('message_type') === 'good' ? 'bg-green-100' : 'bg-red-100';
  @endphp
  <div class="{{ $bgColor }} px-4 py-2 mb-4 rounded-md">
    {!! session('message_content') !!}
  </div>
@endif
