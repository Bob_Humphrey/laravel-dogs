<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="How well can you recognize a dog's breed? Test and expand your knowledge with this app.">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  @livewireScripts
  <script src="{{ asset('js/all.js') }}" defer></script>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>
  <div class="font-nunito_regular">
    <nav class="bg-gray-50 py-2" x-data="{ showMenu: false }">
      <div
        class="flex flex-col xl:flex-row xl:justify-between xl:items-center w-11/12 xl:w-5/6 mx-auto py-3 bg-purple-300 sm:bg-red-300 md:bg-orange-300 lg:bg-yellow-300 xl:bg-gray-50">

        <div class="flex justify-between items-center">

          <div class="flex xl:justify-start">
            <a href="/" class="flex flex-col xl:flex-row w-full">
              <h1 class="font-nunito_extrabold text-3xl text-indigo-700 hover:text-brown-500">
                {{ config('app.name', 'Laravel Application') }}
              </h1>
            </a>
          </div>

          <div class="xl:hidden w-8 cursor-ponunito" @click="showMenu=!showMenu" x-show="!showMenu">
            @svg('menu', 'fill-current')
          </div>

          <div class="xl:hidden w-8 cursor-ponunito" @click="showMenu=!showMenu" x-show="showMenu">
            @svg('close', 'fill-current')
          </div>

        </div>

        <div
          class="hidden xl:flex flex-row justify-end text-base font-nunito_bold xl:text-xl text-indigo-700 leading-none pb-8 xl:pb-0">
          @include('layouts.mainmenu')
        </div>

        <div
          class="flex flex-col xl:hidden text-lg font-nunito_bold xl:text-xl text-indigo-700 leading-none pb-8 xl:pb-0"
          x-show="showMenu">
          @include('layouts.mainmenu')
        </div>

      </div>
    </nav>

    <main class="w-11/12 lg:w-3/4 xl:w-1/2 py-4 mx-auto">
      @include('layouts.flash')
      @yield('content')
    </main>

    @include('layouts.footer')

  </div>
</body>

</html>
