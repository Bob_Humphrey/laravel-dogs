@php
use Illuminate\Support\Str;
$path = request()->path();
$quiz = Str::of($path)->contains('quiz') ? true : false;
$login = Str::of($path)->contains('login') ? true : false;
$results = Str::of($path)->contains('results') ? true : false;
$groups = Str::of($path)->contains('groups') ? true : false;
@endphp

<a href="{{ url('/quiz') }}" class="{{ $quiz ? 'text-brown-500' : '' }} hover:text-brown-500 lg:px-4 py-2">
  Quiz
</a>

@if (user_is_logged_in())
  <a href="{{ url('/results/' . session('student')->id) }}"
    class="{{ $results ? 'text-brown-500' : '' }} hover:text-brown-500 lg:px-4 py-2">
    Results for {{ session('student')->name }}
  </a>
@endif

<a href="{{ url('/groups') }}" class="{{ $groups ? 'text-brown-500' : '' }} hover:text-brown-500 lg:px-4 py-2">
  Groups
</a>

@if (user_is_not_logged_in())
  <a href="{{ url('/login') }}" class="{{ $login ? 'text-brown-500' : '' }} hover:text-brown-500 lg:pl-4 pr-8 py-2">
    Login
  </a>
@endif

@if (user_is_logged_in())
  <a href="{{ url('/logout') }}" class="hover:text-brown-500 lg:pl-4 pr-8 py-2">
    Logout
  </a>
@endif

@livewire('search-bar')
