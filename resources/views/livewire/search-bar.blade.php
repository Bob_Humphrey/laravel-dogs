<div class="relative font-nunito_regular">
  <input type="text" class="py-1 px-3 rounded" placeholder="Search for a Breed..." wire:model="query" />

  @if(!empty($query))
    <div class="fixed top-0 right-0 bottom-0 left-0" wire:click="resetState">
    </div>

    <div class="absolute z-10 w-full bg-white text-base text-gray-900 border-t border-gray-100 rounded-t-none shadow-md">
      @if(!empty($breeds))
        @foreach($breeds as $i => $breed)
          <div class="py-2 px-4 hover:bg-gray-200">
            <a href="{{ route('study', $breed['id']) }}" class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }}">
              {{ $breed['name'] }}
            </a>
          </div>
        @endforeach
      @else
        <div class="list-item">No results!</div>
      @endif
    </div>
  @endif
</div>
