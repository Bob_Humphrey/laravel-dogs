<div class="grid grid-cols-2 py-4">
  <div
    class="flex flex-col items-center justify-center w-full text-2xl text-indigo-800 font-nunito_bold bg-indigo-200 py-10 rounded-l-md">
    {{ $breed->name }}
    <div class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold bg-indigo-200 h-24">
      <button class="btn-white text-base border-none" wire:click="handleSeeAnother">
        See Another
      </button>
    </div>
  </div>
  <div class="flex items-center justify-center w-full bg-brown-200 rounded-r-md overflow-hidden">
    <img src={{ $image }} />
  </div>
</div>
