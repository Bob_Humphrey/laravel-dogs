<div>

  {{-- FEEDBACK --}}

  <div class="{{ $state['handleAnswerMode'] ? '' : 'hidden' }}
    {{ $state['correct'] ? 'text-green-500 ' : 'text-red-500' }}
    grid grid-cols-12 px-4 mb-4">
    <div class="col-span-1 {{ $state['correct'] ? '' : 'hidden' }}">
      <div class="show-all w-12 text-2xl cursor-pointer overflow-visible">
        @svg('checkmark-outline', 'fill-current')
      </div>
    </div>
    <div class="col-span-1 {{ $state['correct'] ? 'hidden' : '' }}">
      <div class="show-all w-12 text-2xl cursor-pointer overflow-visible">
        @svg('close-outline', 'fill-current')
      </div>
    </div>
    <div class="col-span-8 text-lg font-nunito_light leading-tight pr-8">
      {!! $state['feedback'] !!}
    </div>
    <div class="{{ $state['handleAnswerMode'] ? '' : 'hidden' }} col-span-3 flex items-center justify-end">
      <button class="btn-white" wire:click="nextQuestion">
        Next
      </button>
    </div>
  </div>

  {{-- PICTURE AND ANSWER BUTTONS --}}

  <div class="grid grid-cols-2 pb-4">
    <div class="flex items-center bg-brown-200 rounded-l-md overflow-hidden">
      <img src={{ $state['image'] }} />
    </div>
    <div
      class="flex flex-col items-center justify-center w-full  font-nunito_bold bg-indigo-200 py-10 rounded-r-md overflow-hidden">
      @foreach ($state['choices'] as $choice)
        <button
          class="{{ $choice['id'] === $state['userAnswer'] ? 'bg-indigo-800 text-white' : 'bg-white text-indigo-800' }} hover:bg-indigo-800 hover:text-white text-center rounded py-2 my-2 w-64 cursor-pointer focus:outline-none"
          wire:click="handleAnswer({{ $choice['id'] }})">
          {{ $choice['name'] }}
        </button>
      @endforeach
    </div>
  </div>


  <div class="rounded-md overflow-hidden">

    {{-- QUIT BUTTON --}}

    <div class="flex flex-col lg:flex-row items-center justify-center w-full font-nunito_bold h-24">
      <button class="btn-white" wire:click="handleQuitQuiz">
        Quit / Start New Quiz
      </button>
    </div>

    {{-- RESPONSES --}}

    <div class="{{ count($state['responses']) === 0 ? 'hidden' : '' }} flex w-full justify-center mx-auto pb-10">
      <table class="w-full">
        <tr class="grid grid-cols-2 text-sm font-nunito_bold border-b border-gray-300">
          <th class="col-span-2 text-center py-3">Answers for This Quiz Only</th>
        </tr>
        @foreach ($state['responses'] as $response)
          <tr class="grid grid-cols-2 text-sm font-nunito_light border-b border-gray-300">
            <td class="col-span-1 text-left py-3">
              <a href={{ url('study/' . $response['id']) }} class="font-nunito_bold text-brown-500 cursor-pointer">
                {{ $response['breed'] }}
              </a>
            </td>
            <td class="col-span-1 text-right py-3">
              {{ $response['correct'] ? 'Right' : 'Wrong' }}
            </td>
          </tr>
        @endforeach
      </table>
    </div>

  </div>
</div>
