@extends('layouts.app')

@section('content')
  <div class=''>
    @if (user_is_logged_in())
      <div class="mb-6 px-24">
        <img src="{{ asset('img/dog2.png') }}" alt="Logo" />
      </div>
      <div class="text-3xl font-nunito_extrabold text-indigo-700 text-center mb-4">
        You're already logged in!
      </div>
    @endif

    @if ($errors->any())
      <div class="bg-red-100 rounded-md py-2 px-4 mb-4">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    @if (user_is_not_logged_in())
      <div class='text-3xl font-nunito_bold text-indigo-700 mt-2'>Login</div>

      <div class='bg-gray-100 py-16 mt-6 mb-16 rounded-md'>
        <form method="POST" action="/login">
          @csrf

          <div class='grid grid-cols-2 font-nunito_regular text-lg'>
            <label for="username" class='col-span-1 text-right pr-6 py-2'>
              User Name
            </label>

            <div class='col-span-1'>
              <input id="username" type="text" class='px-4 py-2' name="username" value="{{ old('username') }}" required
                autofocus>
            </div>
          </div>

          <div class="flex items-center w-full pt-10">
            <button type="submit" class='btn-white mx-auto'>
              Login
            </button>
          </div>
        </form>
      </div>
    @endif

  </div>
@endsection
