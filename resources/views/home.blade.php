@extends('layouts.app')

@section('content')

  <div class="font-nunito_bold">
    <div class="mb-6 px-24">
      <img src="{{ asset('img/dog2.png') }}" alt="Logo" />
    </div>
    <div class="text-3xl font-nunito_extrabold text-indigo-700 text-center mb-4">
      Test your knowledge of dog breeds with this
      <a href={{ url('/quiz') }} class="underline">
        quiz</a>.
    </div>
    <div class="text-2xl text-gray-500 text-center">
      If you
      <a href={{ url('/login') }} class="underline">
        login</a>
      with a user name, your
      <a href={{ url('/results/' . user_logged_in_id()) }} class="underline">
        results</a>
      will be saved.
    </div>
    <div class="text-2xl text-gray-500 text-center">
      It's easy to study the breeds you don't know. The ones
    </div>
    <div class="text-2xl text-gray-500 text-center mb-4">
      you miss most often are shown at the top of your
      <a href={{ url('/results/' . user_logged_in_id()) }} class="underline">
        results</a>
      list.
    </div>
    <div class="text-lg text-brown-300 text-center">
      Dog Breed Quiz is based on the
      <a href="https://dog.ceo/dog-api/" target="_blank" rel="noopener noreferrer" class="underline">
        Dog API
      </a>
    </div>
    <div class="text-lg text-brown-300 text-center mb-8">
      Illustration by
      <a href="https://dribbble.com/marina-f" target="_blank" rel="noopener noreferrer" class="underline">
        Marina Fedoseenko
      </a>
      from
      <a href="https://icons8.com/" target="_blank" rel="noopener noreferrer" class="underline">
        Icons8
      </a>
    </div>
  </div>


@endsection
