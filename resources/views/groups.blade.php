@extends('layouts.app')

@section('content')

  <div class="mb-8">
    <div class="text-xl font-nunito_bold text-gray-400 text-center my-4">
      You might need a little extra study to learn these breeds.
    </div>

    @include('group', ['subtitle' => 'Lesser Known', 'chunks' => $lesserKnown])

    @include('group', ['subtitle' => 'Hounds', 'chunks' => $hounds])

    @include('group', ['subtitle' => 'Terriers', 'chunks' => $terriers])

    @include('group', ['subtitle' => 'Spaniels', 'chunks' => $spaniels])

    @include('group', ['subtitle' => 'Retrievers', 'chunks' => $retrievers])
  </div>

@endsection
