<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreedsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('breeds', function (Blueprint $table) {
      $table->id();
      $table->string('key', 100)->index();
      $table->string('breed', 50);
      $table->string('subbreed', 50);
      $table->string('name', 100);
      $table->boolean('lesser');
      $table->boolean('hound');
      $table->boolean('terrier');
      $table->boolean('spaniel');
      $table->boolean('retriever');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('breeds');
  }
}
