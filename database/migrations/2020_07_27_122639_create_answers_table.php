<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('answers', function (Blueprint $table) {
      $table->id();
      $table->boolean('right')->default(0);
      $table->boolean('wrong')->default(0);
      $table->decimal('wrong_pct', 3, 2)->index();
      $table->bigInteger('breed_id')->unsigned()->index()->nullable();
      $table->foreign('breed_id')->references('id')->on('breeds');
      $table->bigInteger('user_id')->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('answers');
  }
}
